��    $      <  5   \      0     1  
   ?     J     ^     k     r  	   ~     �     �     �  
   �  	   �     �     �     �     �     �                    5     T     [     `     g     ~     �      �     �     �     �       $   .  	   S     ]  8  s     �  "   �     �  '   �  	        &     2     ;     U     ^     o  "   }     �  >   �  B   �  '   +	     S	     l	     t	     x	  <   �	     �	     �	  
   �	  4   �	     %
     ?
  !   L
     n
  &   �
     �
     �
  +   �
          #                     $                                               "   #      
                                     !                                  	                                Access denied Add a form Administration menu Choose forms Delete Description Directory Directory search Edit Field Field type Form list Label List List of all available forms Manage directory search Manage intervenants Managing Name Number of elements by page Please chose at least one form Rights Save Search Search form management Search in directory Simple text The description can not be empty The directory can not be empty The field can not be empty The label can not be empty The name can not be empty This will remove this form, proceed? View card Who are the managers? Project-Id-Version: directory_search
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-05 15:29+0100
PO-Revision-Date: 2017-12-05 15:29+0100
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <antoine.gallet@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: ds_translate;ds_translate:1,2
X-Generator: Poedit 1.8.7
X-Poedit-SearchPath-0: programs
 Accès refusé Ajouter un formulaire de recherche Menu d'administration Choisissez les formulaires de recherche Supprimer Description Annuaire Recherche dans l'annuaire Modifier Champ d'annuaire Type de champ Liste des formulaires de recherche Libellé Liste de valeur (généré à partir des valeurs disponibles). Afficher la liste de toutes les formulaire de recherche disponible Gestion de la recherche dans l'annuaire Gestion des intervenants Gestion Nom Nombre d'éléments par page Veuilliez sélectionner au moins un formulaire de recherche. Droits Enregistrer Rechercher Gestion des formulaires de recherche dans l'annuaire Recherche dans l'annuaire Texte simple La description ne peux être vide L'annuaire ne peux être vide Le champ d'annuaire ne peux être vide Le libellé ne peux être vide Le nom est obligatoire Cela va supprimer ce formulaire, procéder? Voir la fiche Qui sont les gestionnaires ? 