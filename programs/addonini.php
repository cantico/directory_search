;<?php /*

[general]
name							="directory_search"
version							="1.0.2"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Search in directory"
description.fr					="Recherche dans l'annuaire"
delete							=1
ov_version						="8.5.0"
php_version						="5.4.0"
addon_access_control			="1"
author							="Cantico"
icon							="icon.png"


[addons]
widgets							="1.0.44"
LibTranslate					=">=1.12.0rc3.01"

;*/