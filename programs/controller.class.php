<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';

class ds_Controller extends bab_Controller
{
    protected function getControllerTg()
    {
        return 'addon/directory_search/main';
    }

    /**
     * Get object name to use in URL from the controller classname
     * @param string $classname
     * @return string
     */
    protected function getObjectName($classname)
    {
        $prefix = strlen('ds_Ctrl');
        return strtolower(substr($classname, $prefix));
    }


    /**
     * @return ds_CtrlAdmin
     */
    public function Admin($proxy = true)
    {
        if(!ds_isManager()){
            throw new Exception(ds_translate('Access denied'));
        }
        require_once dirname(__FILE__) . '/ctrl/admin.ctrl.php';
        return bab_Controller::ControllerProxy('ds_CtrlAdmin', $proxy);
    }


    /**
     * @return ds_CtrlForm
     */
    public function Form($proxy = true)
    {
        if(!ds_isManager()){
            throw new Exception(ds_translate('Access denied'));
        }
        require_once dirname(__FILE__) . '/ctrl/form.ctrl.php';
        return bab_Controller::ControllerProxy('ds_CtrlForm', $proxy);
    }


    /**
     * @return ds_CtrlSearch
     */
    public function Search($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/search.ctrl.php';
        return bab_Controller::ControllerProxy('ds_CtrlSearch', $proxy);
    }
}