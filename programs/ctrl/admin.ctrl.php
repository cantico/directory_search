<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/admin.ui.php';

/**
 *
 */
class ds_CtrlAdmin extends ds_Controller
{

    private function addItemMenu($page)
    {
        $page->addItemMenu('home', ds_translate('Administration menu'), ds_Controller()->Admin()->home()->url());
        return $page;
    }

    public function home()
    {		
        $W = bab_Widgets();
        $page = ds_Page();
        $this->addItemMenu($page);
        $page->setCurrentItemMenu('home');

        $HomeFrame = ds_AdminHome();
        $page->addItem($HomeFrame);

        return $page;
    }

    public function rights()
    {
        $W = bab_Widgets();
        $page = ds_Page();
        $this->addItemMenu($page);
        $page->addItemMenu(__METHOD__, ds_translate('Rights'), ds_Controller()->Admin()->rights()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ds_AdminEditor();
        $page->addItem($editor);

        return $page;
    }

    public function saveRights($options = array())
    {
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';

        aclSetRightsString('ds_manager_groups', 1, $options['manager']);

        return true;
    }
}


