<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/form.ui.php';

/**
 *
 */
class ds_CtrlForm extends ds_Controller
{

    public function displayList()
    {
        $W = bab_Widgets();
        $page = ds_Page();
        $page->addItemMenu('home', ds_translate('Administration menu'), ds_Controller()->Admin()->home()->url());
        $page->addItemMenu(__METHOD__, ds_translate('Form list'), ds_Controller()->Form()->displayList()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->forms();

        $page->addItem($Layout);

        return $page;
    }

    public function forms()
    {
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    ds_translate('Add a form'),
                    ds_Controller()->Form()->edit()
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new ds_FormList();
        $set = ds_FormSet();
        $forms = $set->select();
        $forms->orderAsc($set->label);
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($forms);
        $tableView->addDefaultColumns($set);

        $VboxLayout->addItem($tableView);

        $VboxLayout->setReloadAction(ds_Controller()->Form()->forms());

        return $VboxLayout;
    }


    public function edit($id = null)
    {
        $W = bab_Widgets();
        $page = ds_Page();
        $page->addItemMenu('home', ds_translate('Administration menu'), ds_Controller()->Admin()->home()->url());
        $page->addItemMenu('displayList', ds_translate('Form list'), ds_Controller()->Form()->displayList()->url());
        $page->addItemMenu(__METHOD__, ds_translate('Add a form'), ds_Controller()->Form()->edit($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new ds_FormEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function delete($id = null)
    {
        $set = ds_FormSet();
        $set->delete($set->id->is($id));

        if(bab_isAjaxRequest()) {
            return true;
        }
        ds_Controller()->Form()->displayList()->location();
    }

    public function save($form = null)
    {
        $set = ds_FormSet();
        $formORM = false;

        if(isset($form['id']) && $form['id']){
            $formORM = $set->get($form['id']);
        }

        if(!$formORM){
            $formORM = $set->newRecord();
        }

        $formORM->setFormInputValues($form);
        $formORM->save();

        return true;
    }
}
