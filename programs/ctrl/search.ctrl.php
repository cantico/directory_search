<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/search.ui.php';

/**
 *
 */
class ds_CtrlSearch extends ds_Controller
{

    public function displayList($limit = null, $portlet = false, $pageLength = 10)
    {
        $W = bab_Widgets();
        $page = ds_Page();
		$id = 'ds-result-list-'.$page->getId();
		
		unset($_SESSION['ds-search']);
		
		$layout = $W->VBoxItems(
			ds_searchForms($limit, $id),
			$this->searchs($pageLength, $id)
		)->setVerticalSpacing(1, 'em');
		
		if (bab_isAjaxRequest() || $portlet) {
			return $layout;
		}
		
		$page->addItem($layout);

        return $page;
    }
	
	public function saveSearch($form = null)
	{
		$_SESSION['ds-search'] = $form;
		$_SESSION['ds-search']['value'] = bab_getStringAccordingToDataBase($form['value'], bab_Charset::UTF_8);
        $W = bab_Widgets();
		$W->deleteUserConfiguration($form['link'].'/page', 'widgets', true);
		return true;
	}

    public function searchs($pageLength = 10, $id)
    {
        $W = bab_Widgets();
		
		if (!isset($_SESSION['ds-search']) || !isset($_SESSION['ds-search']['value']) || !$_SESSION['ds-search']['value']) {
			return $W->VBoxLayout($id)->setReloadAction(ds_Controller()->Search()->searchs($pageLength, $id));
		}
		$_SESSION['ds-search']['value'] = trim($_SESSION['ds-search']['value']);
		
		$formSet = ds_FormSet();
		$form = $formSet->get($formSet->id->is($_SESSION['ds-search']['id']));
		
		if(!$form) {
			return $W->VBoxLayout($id)->setReloadAction(ds_Controller()->Search()->searchs($pageLength, $id));
		}
		
		$values = array();
		$fields = explode(',', $form->field);

        $tableView = new ds_SearchList();
		$tableView->setId($id);
		
		require_once $GLOBALS['babInstallPath']."utilit/searchapi.php";
		$realm = bab_Search::getRealm('bab_SearchRealmDirectories');
		$realm->setDirectory($form->directory);
	
		$criteria = null;
		foreach($fields as $field) {
			if ($criteria == null) {
				$criteria = $realm->$field->contain($_SESSION['ds-search']['value']);
			} else {
				$criteria = $criteria->_OR_($realm->$field->contain($_SESSION['ds-search']['value']));
			}
			if(in_array($field, ds_getPhoneNumberFields())) {
				$values = ds_getPhoneValues($_SESSION['ds-search']['value']);
				foreach($values as $value) {
					$criteria = $criteria->_OR_($realm->$field->contain($value));
				}
			}
		}

		$results = $realm->search($realm->getDefaultCriteria()->_AND_($criteria));
		//var_dump($realm->searchQuery($realm->getDefaultCriteria()->_AND_($criteria)));
		$header = $realm->getColumnsSettings();
		$header = array('id' => '') + $header;
		
		$content = array();
		foreach($results as $result) {
			$item = array();
			foreach($header as $k => $column) {
				$item[$k] = $result->$k;
			}
			$content[] = $item;
		}
		
		$tableView->addColumnClass(0, 'widget-column-minimal-width');
		$tableView->setAjaxAction();
		$tableView->setPageLength($pageLength);
		$tableView->setHeader($header);
        $tableView->setContent($content);
		$tableView->addClass(Func_Icons::ICON_LEFT_16);

        $tableView->setReloadAction(ds_Controller()->Search()->searchs($pageLength, $id));

        return $tableView;
    }
}
