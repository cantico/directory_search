<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

function ds_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('directory_search');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function ds_loadOrm()
{
    static $loadOrmDone = false;

    if (!$loadOrmDone) {

        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->initMySql();

        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        ORM_MySqlRecordSet::setBackend($mysqlbackend);

        $loadOrmDone = true;
    }
}


/**
 * @return ds_Controller
 */
function ds_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('ds_Controller');
}

function ds_Page()
{
    $addon = bab_getAddonInfosInstance('directory_search');

    $W = bab_Widgets();
    $page = $W->BabPage();

    if(bab_rp('print')){
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $page->addStyleSheet('../../'.$addon->getStylePath().'print.css');
        }else{
            $page->addStyleSheet($addon->getStylePath().'print.css');
        }

    }else{
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $page->addStyleSheet('../../'.$addon->getStylePath().'directory_search.css');
        }else{
            $page->addStyleSheet($addon->getStylePath().'directory_search.css');
        }
    }


    return $page;
}


/**
 * @return bool
 */
function ds_isManager($credential = true)
{
    if($credential){
        bab_requireCredential();
    }
    return bab_isUserAdministrator() || bab_isAccessValid('ds_manager_groups', 1);
}





function ds_IncludeSet()
{
    require_once dirname(__FILE__).'/set/form.class.php';
}

/**
 * @return ds_MeetingTypeSet
 */
function ds_FormSet()
{
    ds_IncludeSet();

    return new ds_FormSet();
}



function ds_getPhoneNumberFields()
{
	return array(
		'btel',
		'mobile',
		'htel',
		'bfax'
	);
}



function ds_getPhoneValues($phone)
{
	$values = array();
	
	$values[0] = str_replace('+', '', $phone);
	$original = $phone;
	if($values[0] != $phone) {
		$original = $values[0];
	} else {
		unset($values[0]);
	}
	$values[1] = str_replace('.', ' ', $original);
	$values[2] = str_replace(' ', '.', $original);
	$values[3] = str_replace('.', '', $original);
	$values[4] = str_replace(' ', '', $original);
	$values[5] = str_replace(array(' ', '.'), '', $original);
	if (strlen($original)>2 && (strlen($original)%2) == 0) {
		$temp = str_split($original, 2);
		$values[6] = implode(' ', $temp);
		$values[7] = implode('.', $temp);
	}
	
	for ($i=1; $i<=7; $i++) {
		if(isset($values[$i])) {
			if($original == $values[$i]) {
				unset($values[$i]);
			}
		}
	}
	
	return $values;
}
