<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org								   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )			  *
 *																	  *
 * This file is part of Ovidentia.									  *
 *																	  *
 * Ovidentia is free software; you can redistribute it and/or modify	*
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";
require_once dirname(__FILE__) . '/functions.php';

function directory_search_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath']."utilit/eventincl.php";

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('directory_search');
    }

    $functionalities = new bab_functionalities();
    $functionalities->unregister('PortletBackend/JoinDelegation');

    return true;
}


function directory_search_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
    require_once $GLOBALS['babInstallPath'].'admin/acl.php';

    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
    require_once dirname(__FILE__).'/set/form.class.php';

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet(new ds_FormSet());
    $synchronize->updateDatabase();

    bab_removeAddonEventListeners('directory_search');
    $addon = bab_getAddonInfosInstance('directory_search');
    if (method_exists($addon, 'addEventListener')) {
        // support for standard addon location in vendor
        $addon->addEventListener('bab_eventPageRefreshed', 'directory_search_onPageRefreshed', 'init.php');
        $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'directory_search_onSiteMapItems', 'init.php');
    }else{
         // old ovidentia before 8.2.0
        bab_addEventListener(
        	'bab_eventPageRefreshed',
        	'directory_search_onPageRefreshed',
        	$addon->getRelativePath().'init.php',
        	'directory_search'
		);
        bab_addEventListener(
            'bab_eventBeforePageCreated',
            'directory_search_onSiteMapItems',
            $addon->getRelativePath().'init.php',
            'directory_search'
        );
    }

    aclCreateTable('ds_manager_groups');


    $functionalities = new bab_functionalities();
    @bab_functionality::includefile('PortletBackend');
    if (class_exists('Func_PortletBackend')) {
        $functionalities->registerClass('Func_PortletBackend_DirectorySearch', $addon->getPhpPath() . 'portletbackend.class.php');
    }

    return true;
}




/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function directory_search_onSiteMapItems(bab_eventBeforeSiteMapCreated $event) {
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includefile('Icons');
	
	$position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
	
    if (bab_isUserAdministrator() || ds_isManager(false)) {
        $link = $event->createItem('directory_search_Admin');
        $link->setLabel(ds_translate('Manage directory search'));
        $link->setLink('?tg=addon/directory_search/main&idx=admin.home');
        $link->setPosition($position);
        $link->addIconClassname(Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE);

        $event->addFunction($link);
    }

    $link = $event->createItem('directory_search_forms');
    $link->setLabel(ds_translate('Search in directory'));
    $link->setLink('?tg=addon/directory_search/main&idx=search.displayList');
    $link->setPosition($position);
    $link->addIconClassname(Func_Icons::ACTIONS_EDIT_FIND_USER);

    $event->addFunction($link);

}





function directory_search_onPageRefreshed()
{
    $addon = bab_getAddonInfosInstance('directory_search');
    $babBody = bab_getBody();
	
	$I = bab_functionality::get('Icons');
	$I->includeCss();
}
