<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
bab_Functionality::includeoriginal('PortletBackend');
bab_Functionality::get('Icons');

/**
 *
 */
class Func_PortletBackend_DirectorySearch extends Func_PortletBackend
{
    public function getDescription()
    {
        return ds_translate("Directory search");
    }

    public function select($category = null)
    {
        return array(
        	'directorysearch' => $this->Portlet_DirectorySearch(
        		'directorysearch',
        		ds_translate('Directory search'),
        		ds_translate('Directory search')
			)
		);
    }



    /**
     * Get portlet definition instance
     * @param	string	$portletId			portlet definition ID
     *
     * @return portlet_PortletDefinitionInterface
     */
    public function getPortletDefinition($portletId)
    {
        return $this->Portlet_DirectorySearch(
        	'directorysearch',
        	ds_translate('Directory search'),
        	ds_translate('Directory search')
		);
    }


    public function Portlet_DirectorySearch($id, $name, $description)
    {
        return new PortletDefinition_DirectorySearch($id, $name, $description);
    }

    /**
     * return a list of action to use for configuration
     * when fired, a parameter named backurl will be added to action, this parameter will contain an url to get back to the main configuration page
     * each action can contain an icon and a title
     *
     * @return multitype:Widget_Action
     */
    public function getConfigurationActions()
    {
            return array();
    }

    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        return array();
    }
}


/////////////////////////////////////////


class PortletDefinition_DirectorySearch implements portlet_PortletDefinitionInterface
{

    private $id;
    private $name;
    private $description;

    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->addon = bab_getAddonInfosInstance('directory_location');
    }

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getDescription()
    {
        return $this->description;
    }


    public function getPortlet()
    {
        return new Portlet_DirectorySearch($this->id, $this->name, $this->description);
    }

    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
    	$addon = bab_getAddonInfosInstance('directory_search');
        return $addon->getStylePath() . 'icon.png';
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
    	$addon = bab_getAddonInfosInstance('directory_search');
        return $addon->getStylePath() . 'icon.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return '';
    }

    public function getConfigurationActions()
    {
        return array();
    }

    public function getPreferenceFields()
    {
    	$W = bab_Widgets();		
		$options = array();
		
		$formSet = ds_FormSet();
		$forms = $formSet->select();
		$forms->orderAsc($formSet->label);
		
		$layout = $W->VBoxItems()->setVerticalSpacing(1, 'em');
		
		foreach ($forms as $form) {
			$layout->addItem(
				$W->LabelledWidget(
					$form->label,
					$W->CheckBox()->setName(array('limit', $form->id))
				)
			);
		}
		$options[] = array(
			'type' => 'int',
			'label' => ds_translate('Number of elements by page'),
			'name' => 'pageLength'
		);
		$options[] = array(
			'type' => 'widget',
			'item' => $W->Section(
				ds_translate('Choose forms'),
				$layout
			)
		);
        
		return $options;
    }
}




class Portlet_DirectorySearch extends Widget_Item implements portlet_PortletInterface
{
    private $id;
    private $name;
    private $description;
    private $options = array();

    /**
     * Instanciates the widget factory.
     *
     * @return Func_Widgets
     */
    function Widgets()
    {
        return bab_Functionality::get('Widgets');
    }




    /**
     */
    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;


    }

    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = $this->Widgets();

        if(empty($this->options) || !isset($this->options['limit'])){
            return $W->Label(ds_translate('Please chose at least one form'))->display($canvas);
        }
		
		$limit = array();
		foreach ($this->options['limit'] as $id => $checked) {
			if($checked) {
				$limit[] = $id;
			}
		}
		$pageLength = 10;
		if(isset($this->options['pageLength']) && $this->options['pageLength'] > 0) {
			$pageLength = $this->options['pageLength'];
		}
		
        $controller = ds_Controller();
		$layout = $controller->Search(false)->displayList($limit, true, $pageLength);

        return $layout->display($canvas);
    }

    public function getName()
    {
        return get_class($this);
    }

    public function getPortletDefinition()
    {
        return new PortletDefinition_DirectorySearch($this->id, $this->name, $this->description);
    }

    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        $this->options = $configuration;
    }

    public function setPreference($name, $value)
    {
        $this->options[$name] = $value;
    }

    public function setPortletId($id)
    {

    }
}