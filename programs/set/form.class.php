<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
ds_loadOrm();

class ds_FormSet extends ORM_MySqlRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('directory')->setDescription('Directory'),
            ORM_SetField('field', ds_Form::getFields())->setDescription('Directory fields to search in'),
            
            ORM_StringField('label')->setDescription('Label'),
            ORM_StringField('description')->setDescription('Description'),
            ORM_EnumField('type', ds_Form::getType())
                ->setDescription('Field type')
        );

    }
}


class ds_Form extends ORM_MySqlRecord
{
    const FIELD_TYPE_STRING = 'string';
    const FIELD_TYPE_LIST = 'list';

    public static function getType()
    {
        return array(
            self::FIELD_TYPE_STRING => ds_translate('Simple text'),
            self::FIELD_TYPE_LIST => ds_translate('List')
        );
    }


	public static function getFields() {
		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
		$options = array();
		foreach ($fields as $k => $field) {
			if($k != 'jpegphoto') {
				$options[$k] = $field['name'];
			}
		}
		
		return $options;
	}
}