<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_TableModelView');


/**
 * Liste des forms d'encombrants
 */
class ds_FormList extends widget_TableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->directory, ds_translate('Directory')));
		$this->addColumn(widget_TableModelViewColumn($set->field, ds_translate('Field')));
		$this->addColumn(widget_TableModelViewColumn($set->label, ds_translate('Label')));

        return $this;
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = ds_Controller()->Form();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case '_edit_':
                return $W->HboxItems(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $controller->edit($record->id)
                    )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setTitle(ds_translate('Edit')),
                    $W->Link(
                        $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->delete($record->id)
                    )->setTitle(ds_translate('Delete'))
                    ->setAjaxAction($controller->delete($record->id), $temp)
                    ->setConfirmationMessage(ds_translate('This will remove this form, proceed?'))
                );
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'directory':
                return bab_getDirInfo($record->directory)['name'];
                break;
        }

        return parent::computeCellTextContent($record, $fieldPath);
    }

}


class ds_FormEditor extends Widget_Form
{
    public function __construct($form = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('form');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->form = $form;

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ds_Controller()->Form()->save())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ds_Controller()->Form()->save())
                        ->setSuccessAction(ds_Controller()->Form()->displayList())
                        ->setFailedAction(ds_Controller()->Form()->edit())
                        ->setLabel(ds_translate('Save'))
                )
            );
        }

        if($this->form && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ds_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ds_Controller()->Form()->delete($this->form)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        if($this->form){
        	$this->setHiddenValue('form[id]', $this->form);
            $set = ds_FormSet();
            $form = $set->get($this->form);
            if($form){
                $this->setValues($form->getFormOutputValues(), array('form'));
            }
        }
    }
	
	
	protected function directory()
	{
		$W = bab_Widgets();
		
		$options = array('' => '');
		$directories = bab_getUserDirectories(false, 0);
		
		foreach ($directories as $directory) {
			$options[$directory['id']] = $directory['name'];
		}
		
		if(count($options) == 2) {
			$this->setHiddenValue('form[directory]', $directory['id']);
			return null;
			//return $W->Hidden(null, 'directory', $directory['id']);
		}
		
        return $W->LabelledWidget(
            ds_translate('Directory'),
            $W->Select()->setOptions($options)->setMandatory(true, ds_translate('The directory can not be empty')),
            'directory'
        )->colon(true);
	}
	
	
	protected function field()
	{
		$W = bab_Widgets();
		
		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
		$options = array();
		foreach ($fields as $k => $field) {
			if($k != 'jpegphoto') {
				$options[$k] = $field['name'];
			}
		}
		
        return $W->LabelledWidget(
            ds_translate('Field'),
            $W->Multiselect()->setMultiColumn(3)->addClass('widget-100pc')->setOptions($options)->setMandatory(true, ds_translate('The field can not be empty')),
            'field'
        )->colon(true);
	}
	
	
	protected function label()
	{
		$W = bab_Widgets();
		
        return $W->LabelledWidget(
            ds_translate('Label'),
            $W->LineEdit()->setSize(50)->setMandatory(true, ds_translate('The label can not be empty')),
            'label'
        )->colon(true);
	}
	
	
	protected function description()
	{
		$W = bab_Widgets();
		
        return $W->LabelledWidget(
            ds_translate('Description'),
            $W->LineEdit()->setSize(50)->setMandatory(true, ds_translate('The description can not be empty')),
            'description'
        )->colon(true);
	}
	
	
	protected function type()
	{
		$W = bab_Widgets();
		
		ds_IncludeSet();
		$options = ds_Form::getType();
		
        return $W->LabelledWidget(
            ds_translate('Field type'),
            $W->Select()->addClass('widget-100pc')->setOptions($options)->setMandatory(true, ds_translate('The field can not be empty')),
            'type'
        )->colon(true);
	}


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($this->directory());
        $this->addItem($this->field());
        $this->addItem($this->label());
        $this->addItem($this->description());
        $this->addItem($this->type());
    }
}