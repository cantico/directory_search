<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_TableArrayView');

class ds_SearchFrom extends Widget_Form
{
	protected $form = null;
	protected $link = null;
	
	function __construct(ds_Form $form, $link, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($id, $layout);
		$this->form = $form;
		$this->link = $link;
		
		$this->setName('form');
		$this->setHiddenValue('form[id]', $this->form->id);
		$this->setHiddenValue('form[link]', $this->link);
		
		$this->addFields();
		$this->setValues();
	}
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		if ($this->form->type == ds_Form::FIELD_TYPE_LIST) {
			$item = $W->Select();
			
			require_once $GLOBALS['babInstallPath']."utilit/searchapi.php";
			$realm = bab_Search::getRealm('bab_SearchRealmDirectories');
			$realm->setDirectory($this->form->directory);
		
			$criteria = $realm->getDefaultCriteria();
			$fields = explode(',', $this->form->field);	
			$results = $realm->search($criteria);
			
			$options = array();
			foreach($results as $result) {
				foreach($fields as $field) {
					$options[$result->$field] = $result->$field;
				}
			}
			
			bab_Sort::asort($options);
			
			$item->setOptions($options);
		} else {
			$item = $W->LineEdit();
		}
		
		$item->addClass('widget-100pc');
		
		$this->addItem(
			$W->HBoxItems(
				$W->LabelledWidget(
					$this->form->label,
					$item,
					'value',
					$this->form->description
				)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                $W->SubmitButton()->setLabel(ds_translate('Search'))->setSizePolicy(Widget_SizePolicy::MINIMUM)
                	->setAjaxAction(ds_Controller()->Search()->saveSearch(), $this->link)
			)->addClass('widget-100pc')->setVerticalAlign('middle')->setHorizontalSpacing(2, 'em')
		);
	}
	
	public function setValues()
	{
		if(isset($_SESSION['ds-search']) && isset($_SESSION['ds-search']['id']) && $_SESSION['ds-search']['id'] == $this->form->id) {
			parent::setValues($_SESSION['ds-search'], array('form'));
		}
	}
}


function ds_searchForms($limit = null, $id)
{
	$W = bab_Widgets();
	
	$layout = $W->VBoxLayout()->addClass('ds-forms')->setVerticalSpacing(1, 'em');
	
	$formSet = ds_FormSet();
	
	if($limit === null || empty($limit)) {
		$forms = $formSet->select();
	} else {
		$forms = $formSet->select($formSet->id->in($limit));
	}
	
	foreach($forms as $form) {
		$layout->addItem(new ds_SearchFrom($form, $id));	
	}
	
	return $layout;
}


/**
 * Liste des searchs d'encombrants
 */
class ds_SearchList extends widget_TableArrayView
{

    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    public function computeCellContent($values, $key, $id)
    {

        $controller = ds_Controller()->Search();
        $W = bab_Widgets();
		

        switch ($key) {
            case 'email':
				if($values[$key]) {
					return $W->Link(
						$W->Icon(
							$values[$key],
							Func_Icons::APPS_MAIL
						),
						'mailto:'.$values[$key]
					);
				}
                break;
            case 'id':
                return $W->Link(
					$W->label()->addClass(Func_Icons::APPS_CONTACTS),
					'?tg=addon/search/main&idx=directories&id='.$values[$key]
				)->setTitle(ds_translate('View card'))->setOpenMode(Widget_Link::OPEN_POPUP);
                break;
			default:
				if(in_array($key, ds_getPhoneNumberFields()) && $values[$key]) {
					return $W->Link(
						$W->Icon(
							$values[$key],
							Func_Icons::OBJECTS_PHONE
						),
						'tel:'.$values[$key]
					);
				}
                break;
        }

        return parent::computeCellContent($values, $key, $id);
    }

}


class ds_SearchEditor extends Widget_Form
{
    public function __construct($search = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('search');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->search = $search;
		if($this->search === null) {
			$this->search = '';
		}

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(ds_Controller()->Search()->save())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(ds_Controller()->Search()->save())
                        ->setSuccessAction(ds_Controller()->Search()->displayList())
                        ->setFailedAction(ds_Controller()->Search()->edit())
                        ->setLabel(ds_translate('Save'))
                )
            );
        }

        if($this->search && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(ds_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    ds_Controller()->Search()->delete($this->search)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        $this->setHiddenValue('search[id]', $this->search);
        if($this->search){
            $set = ds_SearchSet();
            $search = $set->get($this->search);
            if($search){
                $this->setValues($search->getValues(), array('search'));
            }
        }
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                ds_translate('Name'),
                $W->LineEdit()->setMandatory(true, ds_translate('The name can not be empty')),
                'name'
            )->colon(true)
        );
    }
}